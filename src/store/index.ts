import Vue from 'vue';
import Vuex, {
  StoreOptions
} from 'vuex';
import RootState from '@/types/state/RootState';
import {demo} from "@/store/demo";

Vue.use(Vuex);

const store: StoreOptions < RootState > = {
  state: {
    version: '2.0.0' // a simple property
  },
  mutations: {
    updateVersion(state, newVersion){
      state.version = newVersion
    }
  },
  modules: {
    "demo": demo
  }
};

export default new Vuex.Store < RootState > (store);