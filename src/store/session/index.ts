import {
    VuexModule,
    Module,
    Mutation,
    Action
} from "vuex-module-decorators";
import Session from '@/types/Session'

@Module({
    namespaced: true
})
class SessionStore extends VuexModule {
    public session: Session = {
        userId: "abc",
        fullName: "defg"
    }

    @Mutation
    createSession(session: Session) {
        this.session = session
    }
}

export default SessionStore;