import DemoState from "@/types/state/DemoState";
import { Module, ActionTree, MutationTree } from "vuex";
import RootState from "@/types/state/RootState";

const state: DemoState = {
  version: "1.0"
};

const namespaced: boolean = true;

const mutations: MutationTree<DemoState> = {
  updateVersion(state, payload: string) {
    console.log('mutation called')
    state.version = payload;
  }
};

const actions: ActionTree<DemoState, RootState> = {
  updateState({ commit, rootState }, { rootVersion, moduleVersion }): any {
      console.log('action called')
    commit("updateVersion", rootState.version + ":" + moduleVersion);
    commit("updateVersion", rootVersion, { root: true });
  }
};

export const demo: Module<DemoState, RootState> = {
  namespaced,
  state,
  mutations,
  actions
};
