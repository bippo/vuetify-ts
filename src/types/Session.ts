interface Session {
    userId: string,
    fullName: string
}

export default Session