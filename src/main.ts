// https://github.com/vuetifyjs/vue-cli-plugin-vuetify/issues/112
/**
 * In the file tsconfig.json I added the following to the compilerOptions:

 "typeRoots": [
      "./node_modules/@types",
      "./node_modules/vuetify/types"
    ]
and added "vuetify" to "types":

  "types": [
      ....
      "vuetify"
  ]
 */
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
